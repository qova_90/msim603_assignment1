// MSIM603_Program1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "wafer.h"
#include "chip.h"
#include <fstream>
#include <string>

int main()
{
	/* 
	  Initialize any input parameters - chip size, number of defects
	  Call the GenerateDefects() method
	  Count the number of good chips
	  Iterate on number of defects and number of replications (30)
	  Compute the average number of good chips per intial conditions
	*/
	
	float chipSize = 0;
	
	reload:
	std::cout << "\n\n *********************************************************************************" <<
		"\n\n"
		<< " Kindly follow the prompts to enter desired chip size \n";
	std::cout << "Enter chip Size: " << "\t";
	std::cin >> chipSize;

	if (chipSize < 1) goto reload;

	
	int maxIteration = 30;
	int maxNumberDefects = 100;

	int chipCount = 0;
	int waferSize = 12;

	

	// Create and open a file to write data to
	std::ofstream output;
	std::string outputString[4];
	output.open("assignmentReport"+ std::to_string(chipSize)+".csv");
	
	output << "chipSize, IterationCount,numberOfDefects, validChips, goodChips,costPerChip\n";

	int defectIteration = 10;
	// Loop for maximum number of iterations desired
	while (defectIteration <=maxNumberDefects)
	 {
		int iterationCount = 0;
		while (iterationCount <= maxIteration) {
			chipCount = (int)(floor(12 /chipSize));

			Chip* waferLayout = new Chip[chipCount * chipCount];
			//call initialize function
			InitializeWafer(waferLayout, chipCount, chipSize);

			//Apply defects call defect function 
			GenerateDefects(waferLayout, chipCount, chipSize, defectIteration);

			// Count Valid Chips
			int validChips = countValidChips(waferLayout, chipCount);

			//call count good chips
			int goodChips = CountGoodChips(waferLayout, chipCount);

			//Write to file 
			outputString[1] = std::to_string(chipSize);

			std::string finalOutput = std::to_string(chipSize) + "," + std::to_string(iterationCount) + "," +
				std::to_string(defectIteration) + "," + std::to_string(validChips) + "," +
				std::to_string(goodChips) +"," + std::to_string((goodChips>0? (5000 / goodChips):0)) +"\n";

			std::cout << " ChipSize " << chipSize << "iteration " << iterationCount << " defect iteration " << defectIteration
				<< " Valid Chips " << validChips << "Good Chips " << goodChips << " Cost per chip " << (goodChips > 0 ? (5000 / goodChips) : 0) <<std::endl;
			output << finalOutput;

			++iterationCount;
			delete(waferLayout);
		}
		defectIteration+=10;
	}


	output.close();
	

}

