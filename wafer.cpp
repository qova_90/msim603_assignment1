#include "wafer.h"
#include<math.h>
#include <stdlib.h>
#include <random>


/*
  Utility function to check if chips fall within wafer diameter.
*/
 bool ChipFallsWithin(int i,int j,float chipSize,int chipCount) {
    float x = i * chipCount;
    float y = j * chipCount;
    bool a, b, c, d;

    //top Left
    a = sqrt(fabs(pow((x+chipSize)-6, 2) - pow(y-6, 2))) > 6;
    //top right
    b = sqrt(fabs(pow((x + chipSize) - 6, 2) - pow((y+chipSize)- 6, 2))) > 6;
    //lower left
    c = sqrt(fabs(pow(x - 6, 2) - pow(y - 6, 2))) > 6;
    //lower right
    d = sqrt(fabs(pow(x -6, 2) - pow((y + chipSize) - 6, 2))) > 6;

    if (a && b && c && d) return true;
    return false;
}

 /*
  Initialization Process
     for all  i,j
         wafer[i][j] = true if all 4 corners of the chip fall within wafer.

   setting wafer center at (6.0,6.0)
       Upper left ((i+1)*chipSize, j *chipSize))
       Upper right ((i+1)*chipSize, (j+1)*chipSize))
       Lower left (i*chipSize, j*chipSize))
       Lower right (i*chipSize, (j+1) *chipSize))
 */
void InitializeWafer(Chip * waferLayout, int chipCount, float chipSize) {
  //For each wafer, ensure all 4 corners falls within chip diameter
    for (int i = 0; i < chipCount; ++i) {
        for (int j = 0;j < chipCount; ++j) {
            if (ChipFallsWithin(i, j, chipSize, chipCount)) {
                waferLayout[i * chipCount + j].setValid(true);
                waferLayout[i * chipCount + j].setDefect(false);

            }
            else {
                waferLayout[i * chipCount + j].setValid(false);
                waferLayout[i * chipCount + j].setDefect(true);
            }
        }
    }
}

/*
  Check which chips are defective and which are not

  For each defect , generate a random position (x,y)
  Apply the defect at (x,y) to the wafer
  Generate a uniformly distributed random x and y between 0 and array size
  Discard the defect if it is greater than 6cm from the center of the wafer.

*/
//Need to convert(x, y) to index(i, j)
//i = (int)(ceil(x / chipSize))
//j = (int)(ceil(y / chipSize))
//wafer[i][j] = false;

void GenerateDefects( Chip * waferLayout, int chipCount, float chipSize,int defectsCount) {

    //Count number of valid chips
    int numberOfGoodChips = 0;
    for (int i = 0; i < chipCount;++i) {
        for (int j = 0;j < chipCount;++j) {
            if (waferLayout[i * chipCount + j].isValid()) numberOfGoodChips++;
        }
    }
    //Create two Unformly distributed functions using MT Algorithm
    std::random_device rda, rdb;
    std::mt19937 gena(rda()), genb(rdb());
    std::uniform_int_distribution<int> dista(0,99), distb(0,99);
    //Apply defects based on defectsCount 
    int appliedDefects = 0;
    do {
    
        int a = (int)(ceil(dista(genb)/12));
        int b = (int)(ceil(dista(genb)/12));

    //Should unexpected values occur keep within index range
        a = a % chipCount;
        b = b % chipCount;
    //Apply defect to chhip if point falls within its range
    // Check if chip falls within wafer diameter first

        if (waferLayout[a * chipCount + b].isValid()) {
            waferLayout[a * chipCount + b].setDefect(true);
            ++appliedDefects;
        }
    }
        while (appliedDefects < defectsCount);
}

/* Count number of valid chips or chips within wafer diameter
* returns an integer
*/
int countValidChips(Chip* waferLayout, int chipCount) {
    //Count number of valid chips
    int numberOfValidChips = 0;
    for (int i = 0; i < chipCount;++i) {
        for (int j = 0;j < chipCount;++j) {
            if (waferLayout[i * chipCount + j].isValid()) numberOfValidChips++;
        }
    }

    return numberOfValidChips;
}
/*
  Count chips that are valid and also defect free. 
*/
int CountGoodChips(Chip * waferLayout,int chipCount) {

    //Count number of good chips
    int numberOfGoodChips = 0;
    for (int i = 0; i < chipCount; ++i) {
        for (int j = 0;j < chipCount; ++j) {
            if (waferLayout[i * chipCount + j].isGoodChip()) ++numberOfGoodChips;
        }
    }

    //return the number of good chips
    return numberOfGoodChips;
}



