#pragma once

/*
* Jewel Ossom
* 
 Class Definition for chip
 has two members and 5 methods
 each member has a getter and setter
 last method computers for validity and no defect
*/

class Chip
{
	bool valid;
	bool defect;
    
public:
	bool isValid();
	bool isDefected();
	bool isGoodChip();
	void setValid(bool value);
	void setDefect(bool value);
};

