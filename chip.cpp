#include "chip.h"


//Function: check if chip is within wafer diameter
bool Chip::isValid() {
	return valid;
}

//Function: Hold true if chip area contains a defect
bool Chip::isDefected() {
	return defect;
}

//Function: return account of validity and defect state
bool Chip::isGoodChip() {
	return valid && !defect;
}

//Function: setter for valid member
void Chip::setValid(bool value) {
	valid = value;
}

//Function: setter for defect member
void Chip::setDefect(bool value) {
	defect = value;
}

