#pragma once
#include "chip.h"
/*
 Jewel Ossom
 Header Definition of Wafer functions
*/

// Function: Utility function to check if chip falls within wafer diameter
bool ChipFallsWithin(int i, int j, float chipSize, int chipCount);
// Functionality: Initialize wafer and properties
void InitializeWafer(Chip * waferLayout, int chipCount, float chipSize);

// Functionality: Generate number of defects
void GenerateDefects(Chip * waferLayout, int chipCount, float chipSize, int defectsCount);

// Functionality:  Count the number of good chips (valid and non defective)
int CountGoodChips(Chip * waferLayout, int chipCount);

// Functionality: Count number of valid chips or chips within wafer diameter
int countValidChips(Chip* waferLayout, int chipCount);

